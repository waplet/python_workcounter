# README #

This is instruction for running the workCounter

### What is this repository for? ###

* workCounter counts the minutes you spent since running the app, and can give basic stats for specific month
* 0.1

### How do I get set up? ###

* clone repository
* add ./runnerWorkCounter.sh to startup , see popping terminal with your time counter everyday you arrive
* ./runnerStats.py to see some stats about your work time

"-" - you should suspend "workCounter" terminal right before you're "leaving".

### WorkCounter usage
![Snapshot_2016_3_22_13_47_43.png](https://bitbucket.org/repo/8dXnL9/images/2352671156-Snapshot_2016_3_22_13_47_43.png)

### Stats main view
![Snapshot_2016_3_22_13_48_6.png](https://bitbucket.org/repo/8dXnL9/images/3249898652-Snapshot_2016_3_22_13_48_6.png)

### Stats for specific month
![Snapshot_2016_3_22_13_49_6.png](https://bitbucket.org/repo/8dXnL9/images/1835867922-Snapshot_2016_3_22_13_49_6.png)